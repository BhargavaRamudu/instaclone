const multiparty = require('multiparty');
const fs = require('fs');

parseForm = (req, res, next) => {
       
    const form = new multiparty.Form();
	
	form.parse(req, (err, fields, files) => {
        
        res.locals.formFields = fields;
        res.locals.formFields.content = fields.content;
        console.log(fields.content[0]);
        console.log(res.locals.formFields.content[0]);
        if(files > 1) {
            return res.status.json({
                message: "please upload one image at a time"
            })
        }
        if(files.image[0].headers['content-type'] == ('image/png' || 'image/jpeg' ||'image/jpg' || 'image/gif')) {
            const fileData =  fs.createReadStream(files.image[0].path);
            res.locals.files = files;
            res.locals.fileData = fileData;
            console.log('parseForm end')
            console.log(res.locals.files);
            console.log(res.locals.files.image[0].headers);
            next()
        } else {
            return res.status(403).json({message: 'we allow images only to upload'})
            
            
        }
       
        
        
        
       
    })  
     
    
}

module.exports = parseForm;