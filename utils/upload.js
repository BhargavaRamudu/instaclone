const AWS = require('aws-sdk');

const bucketName = "YOUR BUCKET NAME"
const s3 = new AWS.S3({
    accessKeyId: "YOUR ACCESS ID",
    secretAccessKey: "YOUR SECRET ACCESS KEY",
    Bucket: "YOUR BUCKET NAME"
  });

const upload = (req, res, next) => {
  
    const image = {
        Bucket: `${bucketName}/uploads`,
        Key: Date.now() + '-' + res.locals.files.image[0].originalFilename,
        Body: res.locals.fileData,
        ACL: 'public-read'
    }

    s3.upload(image, (err, data) => {
        if(err) {
            return res.json({
                error: 'upload error' + err
            })
        } else {
            console.log(data);
            res.locals.uploadData = data;
            next()
        }       
    })
}

module.exports = upload;