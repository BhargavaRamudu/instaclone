const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const postroutes = require('./routes/posts');
const userroutes = require('./routes/users');
dotenv.config();

app.use(morgan('combined'));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

mongoose.Promise = global.Promise;

app.use((req, res, next) => {
	
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Accept, Content-Type, Authorization')
	if (req.method === 'OPTIONS') {
		res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
		return res.status(200).json({});
	};
	next();
})


mongoose.connect(process.env.DATABASE_CONNECTION_URL, 
				{ useNewUrlParser: true }
				)
				.then(result => {
					console.log('success');

				})
				.catch(err => {
					console.log(err);
				});
app.use('/users', userroutes);

app.use('/posts', postroutes);

app.use((req, res, next) => {
	const error = new Error('Not found');
	error.status = 404;
	next(error);
}) 


app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
		error: {
			message: error.message
		}
	});
});

module.exports = app;