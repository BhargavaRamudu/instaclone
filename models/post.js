const mongoose = require('mongoose');

const postSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    content: {type: String},
    image: { type: String, require: true }
});



module.exports = mongoose.model('Post', postSchema);