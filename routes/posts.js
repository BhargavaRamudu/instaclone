const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const checkLogin  = require('../utils/checkLogin');
const parseForm  = require('../utils/parseForm');
const upload = require('../utils/upload');
const Post = require('../models/post');

router.get('/',checkLogin, (req, res, next) => {
	Post.find()
		.select('content image _id')
		.exec()
		.then(result => {
			const response = {
				count: result.length,
				posts: result.map(post => {
					
					return {
						content: post.content,
						_id: post._id,
						image: {
							type: 'GET',
							url: `${post.image}`
						}
					}
				})
			}
			res.status(200).json(response);
			
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				message: "cannot fetch the posts"
			})
		})
	
});

router.post('/',checkLogin, parseForm, upload,  (req, res, next) => {

	const post = new Post({
		_id: new mongoose.Types.ObjectId(),
		content: res.locals.formFields.content,
		image: res.locals.uploadData.Location
	});
	post
		.save()
		.then(result => {
			res.status(201).json({
				createdPost: result
			});	
		})
		.catch(err => {
			res.status(500).json({
				error: "this is the error"
			})
			
		})
	
});

router.get('/:postId',checkLogin, function(req, res, next) {
	const id = req.params.posttId;
	Post.findById(id)
		.exec()
		.then(result => {	
			if(result) {
				res.status(200).json(result);
			} else {
				res.status(404).json({
					message: "no valid entry for provided Id"
				})
			}
		})
		.catch(err => {
			console.log(err)
			res.status(500).json({error: err});

		});
})


router.patch('/:postId',checkLogin, (req, res, next) => {

	const updateOps = {};
	for (const ops of req.body) {
		updateOps[ops.propName] = ops.value;
	}
	Product.update({_id: req.params.postId}, { $set: updateOps })	
	.exec()
	.then(result => {
		res.status(200).json({
			message: 'updated',
			post: {
				request: 'GET',
				url: `localhost:3000/posts/${req.params.postId}`
			}

		});
	})
	.catch(err => {
		res.status(500).json({ error: err});		
	})	
})




router.delete('/:postId',checkLogin, (req, res, next) => {
	Post.remove({_id : req.params.postId})
		.exec()
		.then(result => {
			res.status(200).json(result);
		})
		.catch(err => {
			console.log(err);
			res.status(500).json(err);
		})
})



// router.delete('/', checklogin, (req, res, next) => {
// 	Product.find({})
// 		.exec()
// 		.then(products => {
// 			const totalproducts = products.length;
// 			let i = 0;
// 			products.map(product => {
// 				Product.remove({_id: product._id})
// 					.exec()
// 					.then(result => {
// 						i++;
// 					})
// 					.catch(err => {
// 						console.log(err);
// 					})
// 			})

// 		})
// 		.catch(err => {
// 			console.log(err);
// 		})
// })

module.exports = router;